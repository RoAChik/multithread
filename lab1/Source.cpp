#define _CRT_SECURE_NO_WARNINGS
#include <fstream>
#include <iostream>
#include <cstring>
#include <vector>
#include <omp.h> 
#include <time.h>
#include <windows.h>

using namespace std;
class Matrix
{
private:
	int lines;
	int cols;
	int ** matrix;

	void resize(int lines, int cols)
	{
		this->lines = lines;
		this->cols = cols;
		matrix = new int *[lines];
		for (int i = 0; i < lines; i++)
		{
			matrix[i] = new int[cols];
		}
		
	}

public:
	~Matrix()
	{
		/*for (int i = 0; i < this->lines; i++)
		{
			delete [] matrix[i];
		}
		delete [] matrix;*/
	}
	int getCols()
	{
		return cols;
	}

	int getLines()
	{
		return lines;
	}

	int get(int i, int j)
	{
		if (i >= lines || j >= cols) throw exception("Bound exceprion");
		return matrix[i][j];
	}

	friend ostream& operator<< (ostream& f, Matrix& m)
	{
		for (int i = 0; i < m.getLines(); i++)
		{
			for (int j = 0; j < m.getCols(); j++)
			{
				f << m.get(i, j) << " ";
			}
			f << endl;
		}
		return f;
	}

	friend istream& operator>>(istream& f, Matrix& m)
	{
		int i = 0;
		int j = 0;
		bool no = false;
		while (!f.eof())
		{
			char buffer[99999];
			f.getline(buffer, sizeof(buffer));
			char * strs = strtok(buffer, " ");
			while (!no && strs != NULL)
			{
				strs = strtok(NULL, " ");
				j++;
			}
			no = true;
			i++;
		}
		m.resize(i, j);
		f.clear();
		f.seekg(0);
		i = 0;
		while (!f.eof())
		{
			char buffer[99999];
			f.getline(buffer, sizeof(buffer));
			char * strs = strtok(buffer, " ");
			int j = 0;
			while (strs != NULL)
			{
				m.matrix[i][j] = atoi(strs);
				strs = strtok(NULL, " ");
				j++;
			}
			i++;
		}
		return f;
	}

	Matrix operator*(const Matrix& a)
	{
		if (this->cols != a.lines) throw exception("lines != cols");
		Matrix m;
		m.resize(this->lines, a.cols);
		int i, j, z = 0;
#pragma omp parallel for schedule(dynamic) private(i,j,z)
		for (i = 0; i < m.lines; i++) {
			for (j = 0; j < m.cols; j++) {
				m.matrix[i][j] = 0;
				for (z = 0; z < m.lines; z++)
				{
					m.matrix[i][j] += this->matrix[i][z] * a.matrix[z][j];
				}
			}
		}
		return m;
	}
};

void main(int argc, char* argv[])
{
	time_t time1, time2;
	if (argc != 3) return;
	Matrix m1;
	fstream f(argv[1]);
	f >> m1;
	f.close();
	Matrix m2;
	fstream f2(argv[2]);
	f2 >> m2;
	f2.close();
	Matrix m;
	try
	{
		time(&time1);
		m = m1*m2;
		time(&time2);
		cout << "time: " << difftime(time2, time1) << endl;
	}
	catch (exception e) {
		cout << "Error: " << e.what() << endl;
	}

	ofstream out("out.txt");
	/*for (int i = 0; i < m.getLines(); i++)
	{
		for (int j = 0; j < m.getCols(); j++)
		{
			out << m.get(i, j) << " ";
		}
		out << endl;
	}*/
	out << m;
	out.close();


}