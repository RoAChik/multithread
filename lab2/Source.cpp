#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cmath>
#include <fstream>
#include <mpi.h>
#include <time.h> 

using namespace std;

int id;
int sizes;
class Jacobi
{
public:
	int N;
	double eps;
	double *X;
	Jacobi(char* f1, char* f2, char* epss)
	{
		eps = std::atof(epss);
		readMatrix(f1);
		readStarts(f2);
	}
private:
	double *A;
	double *F;
	void init(int n)
	{
		N = n;
		if (id == 0) {
			A = new double[n*n];
			F = new double[n];
		}
		X = new double[n];
	}

	double * run(int N, double* A, double* F, double* X)
	{
		double* TempX = new double[N];
		double norm, max;
		int RowNum = N / sizes;
		double* pProcRowsA;
		double* pProcRowsF;
		double* pProcTempX;
		double * tX;
		pProcRowsA = new double[RowNum*N];
		pProcRowsF = new double[RowNum];
		pProcTempX = new double[RowNum];
		tX = new double[N];
		MPI_Bcast(X, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		MPI_Scatter(A, RowNum*N, MPI_DOUBLE, pProcRowsA, RowNum*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		MPI_Scatter(F, RowNum, MPI_DOUBLE, pProcRowsF, RowNum, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		do {
			for (int i = 0; i < RowNum; i++) {
				pProcTempX[i] = 0;
				for (int j = 0; j < N; j++) {
					if ((id*RowNum + i) != j) {
						pProcTempX[i] += pProcRowsA[i*N + j] * X[j];
					}
				}
				pProcTempX[i] = (pProcRowsF[i] - pProcTempX[i])/pProcRowsA[id*RowNum + i*N + i];
			}
			
			norm = fabs(X[id*RowNum] - pProcTempX[0]);
			for (int i = 0; i < RowNum; i++) {
				if (fabs(X[id*RowNum + i] - pProcTempX[i]) > norm)
					norm = fabs(X[id*RowNum + i] - pProcTempX[i]);
			}
			
			MPI_Reduce(&norm, &max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
			MPI_Bcast(&max, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			MPI_Allgather(pProcTempX, RowNum, MPI_DOUBLE, X, RowNum, MPI_DOUBLE, MPI_COMM_WORLD);
		} while (max > eps);
		delete[] pProcTempX;
		return X;
	}
	void readMatrix(string fname)
	{
		ifstream file(fname);
		int n, m;
		file >> m;//3
		file >> n;//4
		init(m);
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				if (j == (n - 1)) file >> F[i];
				else file >> A[i*N + j];
			}
		}

		file.close();
	}

	void readStarts(string fname)
	{
		ifstream file(fname);
		int m;
		file >> m;
		for (int i = 0; i < m; i++)
		{
			file >> X[i];
		}
		file.close();
	}
public:
	void start()
	{
		this->X = run(N, A, F, X);
	}
};

void main(int argc, char* argv[]) {
	if (argc != 5) return;

	Jacobi j(argv[1], argv[2], argv[3]);
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &sizes);
	MPI_Comm_rank(MPI_COMM_WORLD, &id);
	time_t begin, end;
	time(&begin);
	j.start();
	ofstream out(argv[4]);
	if (id == 0) for (int i = 0; i < j.N; i++) out << j.X[i] << endl;
	out.close();
	MPI_Finalize();
}
